#!/bin/bash

set -e

if [ "$1" = 'falco' ]; then
    exec /usr/bin/falco
fi

exec "$@"
