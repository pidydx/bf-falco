#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Set version pins
ENV FALCO_VERSION=0.38.2
ENV FALCO_DRIVER_CHOICE="modern_ebpf"
ENV FALCOCTL_ENABLED="no"

# Set base packages
ENV BASE_PKGS falco=${FALCO_VERSION} libelf1t64

# Update users and groups
RUN userdel ubuntu

# Update and install base packages
COPY etc/apt /etc/apt

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/
ENV HOST_ROOT="/host"
EXPOSE 8765/tcp
VOLUME ["/etc/falco", "/host/run/containerd/containerd.sock", "/host/proc", "/host/etc"]

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["falco"]
